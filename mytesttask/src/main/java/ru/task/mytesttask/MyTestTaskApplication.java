package ru.task.mytesttask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyTestTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyTestTaskApplication.class, args);
	}

}
