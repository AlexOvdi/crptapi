package ru.task.mytesttask.service;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.util.StopWatch;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import ru.task.mytesttask.service.dto.ApiDto;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@Data
public class Crpt {

    private int requestLimit;
    private TimeUnit timeUnit;
    private WebClient crptClient;
    private final Object monitor;
    private AtomicInteger increment;
    private double totalTimeSeconds;

    public Crpt(TimeUnit timeUnit,
                @Qualifier("crtpWebClient") WebClient webClient,
                int requestLimit) {
        this.requestLimit = requestLimit;
        this.totalTimeSeconds = 0;
        this.monitor = new Object();
        this.timeUnit = timeUnit;
        this.crptClient = webClient;
        this.increment = new AtomicInteger(0);
    }

    public synchronized void tryToCallApi(ApiDto apiDto) {
        StopWatch sw = new StopWatch();
        sw.start();
        try {
            synchronized (monitor) {
                if (increment.get() >= requestLimit && requestLimit >= totalTimeSeconds) {
                    double endInterval = requestLimit - totalTimeSeconds;
                    long endLimitApiCall = Math.round(Math.ceil(endInterval));
                    timeUnit.sleep(endLimitApiCall);
                    increment.set(0);
                }
            }
            callApi(apiDto);
            sw.stop();
            totalTimeSeconds = requestLimit - sw.getTotalTimeSeconds();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private void callApi(ApiDto apiDto) {
        crptClient.post()
                .uri("/v3/lk/documents/create")
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(apiDto))
                .retrieve()
                .bodyToMono(Void.class)
                .block();
    }
}