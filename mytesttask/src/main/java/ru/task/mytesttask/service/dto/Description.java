package ru.task.mytesttask.service.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Description {
    private String participantInn;
}